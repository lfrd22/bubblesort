// Code Guidelines: https://developer.mozilla.org/en-US/docs/MDN/Guidelines/Code_guidelines/JavaScript

let barsDemo;
let barsUebung;
let numberOfBars = 10;
let maxValue = 20;
let demoSection = document.getElementById("demo-animation");
let uebungSection = document.getElementById("uebung-animation");
const arrayDemo = [];
const arrayUebung = [];
let barcounterDemo = 0;
let barcounterUebung = 0;
let lastIndexDemo = 10;
let lastIndexUebung = 10;
let thisBar, nextBar = 0;
let thisDiv, nextDiv = null;
let tlDemo = gsap.timeline()
let tlUebung = gsap.timeline();
let tlIcons = gsap.timeline({repeat: -1, repeatDelay: 4});
let tlTitles = gsap.timeline({repeat: -1})
let choice = 0;
let iconVergleichen;
let iconTauschen;
let fertigDiv;
let fertigTrophy;
let fertigText;
let vergleichCode;
let vergleichCodeKlammern;
let tauschCode;
let loopCode;
let loopKlammern;
let highlightColor = "#FEA82F"

window.addEventListener("DOMContentLoaded", () => {
  setupArray(demoSection);
  setupArray(uebungSection);
})

//Eventlistener für alle Buttons, chronologisch
let btnNav = document.getElementById("burger");
btnNav.addEventListener("click", function() {
  showDropdown();
})

let btnNavItems = document.querySelectorAll(".nav-item-mobile");
btnNavItems.forEach(navItem => {
  navItem.addEventListener("click", function() {
    showDropdown();
  })
})

let btnNext = document.getElementById("btn-next-step");
btnNext.addEventListener("click", nextStep);

let btnAutoplay = document.getElementById("btn-autoplay");
btnAutoplay.addEventListener("click", autoplay);

let btnStop = document.getElementById("btn-stop");
btnStop.addEventListener("click", function() {
  tlDemo.pause();
  gsap.to(btnNext, {opacity: 1, duration: 0.1})
  btnAutoplay.removeEventListener("click", autoplay);
})

let btnReloadDemo = document.getElementById("btn-reload-demo");
btnReloadDemo.addEventListener("click",function() {
  window.location.reload();
})

let btnInfo = document.getElementById("btn-info");
btnInfo.addEventListener("click",showInfo);

let btnSwap = document.getElementById("btn-swap");
btnSwap.addEventListener("click", function() {
  if(!tlUebung.isActive()) {
    choice = 1;
    if(animiereUebung()) {
      gsap.to(btnSwap, {opacity: 0.2, duration: 0.1})
      gsap.to(btnDontSwap, {opacity: 0.2, duration: 0.1});
      if(barcounterUebung === 0) {
        tlUebung.call(highlightBars, [barsUebung[0], barsUebung[1]]);
      } else {
        tlUebung.call(highlightBars, [barsUebung[barcounterUebung-1], barsUebung[barcounterUebung+1]]);
      }
    }
  }
})

let btnDontSwap = document.getElementById("btn-dont-swap");
btnDontSwap.addEventListener("click", function () {
  if(!tlUebung.isActive()) {
    choice = 2;
    if(animiereUebung()) {
      gsap.to(btnSwap, {opacity: 0.2, duration: 0.1})
      gsap.to(btnDontSwap, {opacity: 0.2, duration: 0.1});
      if(barcounterUebung === 0) {
        tlUebung.call(highlightBars, [barsUebung[0], barsUebung[1]]);
      } else {
        tlUebung.call(highlightBars, [barsUebung[barcounterUebung], barsUebung[barcounterUebung+1]]);
      }
    }
    }
})

let btnReloadUebung = document.getElementById("btn-reload-uebung");
btnReloadUebung.addEventListener("click",function() {
  window.location.reload();
})

function nextStep() {
  btnAutoplay.addEventListener("click", autoplay);
  tlDemo.resume()
      .eventCallback("onComplete", function() {
        gsap.to(btnAutoplay, {opacity: 1, duration: 0.1})
        gsap.to(btnNext, {opacity: 1, duration: 0.1})
      })
      .timeScale(1);
  if (!tlDemo.isActive()) {
    animiereDemo();
    gsap.to(btnAutoplay, {opacity: 0.2, duration: 0.1})
    gsap.to(btnNext, {opacity: 0.2, duration: 0.1});
  }
}

function autoplay() {
  if (!tlDemo.isActive() && !tlUebung.isActive()) {
    gsap.to(btnAutoplay, {opacity: 0.2, duration: 0.1})
    gsap.to(btnNext, {opacity: 0.2, duration: 0.1});
    animiereDemo();
    tlDemo
        //.call(animiereDemo)
        .eventCallback("onComplete", animiereDemo)
        .timeScale(2);
  }
}

//Zeigt Infokasten der Übung
function showInfo() {
  document.getElementById("infokasten").classList.toggle("showInfo");
}

//Zeigt Navigation in der mobilen Ansicht
function showDropdown() {
  document.getElementById("popup").classList.toggle("show");
}

//Schreibt Sätze in den Titel
const titles = ["Sorting those Bubbles!", "if zahl[k] >  zahl[k+1]","swap (zahl[k], zahl[k+1])", "Bubbles!!!!", "O(n^2)", "I like Bubbles :)", "Du musst runter scrollen!", ":)", "Hello World!","console.log (\"unsere User sind die besten!\");","O(g(n)) := {f(n)∃k>0, n0:∀n ≥n0:f(n)≤k∙g(n)}","Algorithmen machen Spaß!","Sortieren, Whohooo!","function sort(arrayDemo);","Swap it like it's hot!","Vergleichsbasiertes Sortieren"]
gsap.to(".cursor",{opacity:0, ease: "power2.inOut", repeat: -1});
titles.forEach(word => {
  let tl = gsap.timeline({repeat: 1, yoyo: true, repeatDelay: 3})
  tl.to(".text", {duration: 1, text: word})
  tlTitles.add(tl)
})

//Navigation und Übung verschwinden, wenn Klick außerhalb
window.onclick = function(event) {
  if(!event.target.matches(".info-button")) {
    let infos = document.getElementById("infokasten");
    infos.classList.remove("showInfo");
  }
  if(!event.target.matches(".burger")) {
    let dropdowns = document.getElementById("popup");
    dropdowns.classList.remove("show");
  }
}

//Scrolltrigger für Titel und Nav
gsap.from(".header-inside", {duration: 1, y: -200, ease: "power3.out"})
gsap.from(".logo", {duration: 1, scale: 0, ease: "power3.out"})

//scrolltrigger für Sections und Nav Markierungen
const navLinks = gsap.utils.toArray(".nav-link");
const containers = gsap.utils.toArray(".panel");
const triggerClasses = gsap.utils.toArray(".kapitel-container");
containers.forEach((container, i) => {
  ScrollTrigger.create( {
    trigger: triggerClasses[i],
    start:"top bottom",
    end: "bottom center",
    toggleActions: "play complete",
    onEnter: () => {
      gsap.from (triggerClasses[i], {y: 100, duration: 2, ease: "power3.out"})
    }
  })

  ScrollTrigger.create( {
    trigger: triggerClasses[i],
    start:"top 35%",
    end: "bottom 35%",
    toggleActions: "play complete",
    onEnter: () => {
      gsap.to(navLinks[i], {background: highlightColor, duration: 0})
      if(i > 0) {
        gsap.to(navLinks[i-1], {background: "", duration: 0})
      }
      if(i === 4) {
        gsap.to(navLinks[i], {borderRadius: "0 20px 20px 0", duration: 0})
      }
    },
    onEnterBack:() => {
      gsap.to(navLinks[i], {background: highlightColor, duration: 0});
      gsap.to(navLinks[i+1], {background: "", duration: 0})
    },
    onLeave:() => {
      gsap.to(navLinks[i], {background: "", duration: 0})
    },
    onLeaveBack:() => {
      gsap.to(navLinks[i], {background: "", duration: 0})
    }
  })
})

//scrolltrigger MausFadeout
ScrollTrigger.create( {
  trigger: ".maus",
  start: "top bottom",
  end: "bottom 70%",
  onEnter: () => gsap.to (".maus-outer", {opacity: 1, duration: 1.5, ease: "power3.out"}),
  onEnterBack: () => gsap.to (".maus-outer", {opacity:1, duration: 1.5, ease: "power3.out" }),
  onLeave: () => gsap.to (".maus-outer", {opacity: 0, duration: 1.5, ease: "power3.out"}),
  onLeaveBack: () => gsap.to (".maus-outer", {opacity: 0, duration: 1.5, ease: "power3.out"})
})

//scrolltrigger Pfeil nach oben
ScrollTrigger.create( {
  trigger: "#kapitel-ueber",
  start: "top bottom",
  end: "bottom 70%",
  onEnter: () => gsap.to (".upPorter", {opacity: 0.75, duration: 1.5, ease: "power3.out"}),
  onLeaveBack: () => gsap.to (".upPorter", {opacity: 0, duration: 1.5, ease: "power3.out"})
})

//Animation Bubbles in "Über"
gsap.to(".bubble1", {y: -250, repeat: -1, yoyo: true, repeatDelay: 4, duration: 2, ease: "back.inOut"})
gsap.to(".bubble2", {y: -90, repeat: -1, yoyo: true, repeatDelay: 4, duration: 2, ease: "back.inOut"})
gsap.to(".bubble3", {y: -150, repeat: -1, yoyo: true, repeatDelay: 4, duration: 2, ease: "back.inOut"})
gsap.to(".bubble4", {y: -50, repeat: -1, yoyo: true, repeatDelay: 4, duration: 2, ease: "back.inOut"})
gsap.to(".bubble5", {y: -180, repeat: -1, yoyo: true, repeatDelay: 4, duration: 2, ease: "back.inOut"})
gsap.to(".bubble6", {y: -100, repeat: -1, yoyo: true, repeatDelay: 4, duration: 2, ease: "back.inOut"})
gsap.to(".bubble7", {y: -0, repeat: -1, yoyo: true, repeatDelay: 4, duration: 2, ease: "back.inOut"})
gsap.to(".bubble8", {y: -180, repeat: -1, yoyo: true, repeatDelay: 4, duration: 2, ease: "back.inOut"})
gsap.to(".bubble9", {y: -180, repeat: -1, yoyo: true, repeatDelay: 4, duration: 2, ease: "back.inOut"})
gsap.to(".bubble10", {y: -50, repeat: -1, yoyo: true, repeatDelay: 4, duration: 2, ease: "back.inOut"})

//Animation der Icons in "Funktion"
tlIcons.to(".lineal-icon", {x: -20, scale: 0.7})
    .to(".layer-icon", {scale: 1.1},"<")
    .to(".lineal-icon", {x: 0, scale: 1})
    .to(".layer-icon", {scale: 1}, "<")
    .to(".suche-icon", {scale: 0.7}, "< 1")
    .to(".suche-icon", {scale: 1}, ">")
    .to(".pfeil-oben", {
      ease: "power3.out", x: "-=58",
      modifiers: {x: gsap.utils.unitize(x => parseFloat(x) % 58)}}, "< 1")
    .to(".pfeil-unten", {ease: "power3.out", x: "+=58",
      modifiers: {x: gsap.utils.unitize(x => parseFloat(x) % 58)}}, "<")
    .to(".loop-icon", {rotation: 360}, "< 1")

//erstellt random Arrays für Demo und Übung
function setupArray(area) {
  let parentArea = area;
  if(window.innerWidth < 960) {
    numberOfBars = 8;
    lastIndexDemo = numberOfBars-2;
    lastIndexUebung = numberOfBars-2;
  } else {
    lastIndexDemo = numberOfBars-2;
    lastIndexUebung = numberOfBars-2;
  }
  for(let i = 0; i < numberOfBars; i++) {
    let random = Math.floor((Math.random() * maxValue) +1)
    if(parentArea === demoSection) {
      arrayDemo.push(random);
    } else {
      arrayUebung.push(random);
    }
    let node = document.createElement("div");
    let height = random * 100 / maxValue;
    node.innerText = random;
    node.setAttribute("class", "bar");
    node.setAttribute("data-value", random);
    node.setAttribute ("style", "height: " +height+ "%;");
    if(parentArea === uebungSection) {
      if(i < 2) {
        node.style.backgroundColor = highlightColor;
      }
    }
    parentArea.appendChild(node);
  }
}

//Demo iterieren und animieren
function animiereDemo() {
  barsDemo = demoSection.querySelectorAll(".bar");
  thisBar = arrayDemo[barcounterDemo];
  nextBar = arrayDemo[barcounterDemo + 1];
  thisDiv = barsDemo[barcounterDemo];
  nextDiv = barsDemo[barcounterDemo + 1];
  iconVergleichen = document.getElementById("vergleich-icon");
  fertigDiv = document.getElementById("trophy-icon");
  fertigTrophy = document.getElementById("fertig-icon");
  fertigText =document.getElementById("fertig-text");
  iconTauschen = document.getElementById("tauschen-icon");
  vergleichCode = document.getElementById("vergleich-code");
  vergleichCodeKlammern = document.getElementById("vergleich-code-klammern");
  tauschCode = document.getElementById("tausch-code");
  loopCode = document.getElementById("loop-code");
  loopKlammern = document.getElementById("loop-code-klammern");

  if(lastIndexDemo < 0) {
    fertigDiv.style.opacity = 1;
    fertigTrophy.style.color= highlightColor;
    fertigText.style.color= highlightColor;
    vergleichCode.style.opacity = 1;
    vergleichCodeKlammern.style.opacity = 1;
    tauschCode.style.opacity = 1;
    loopCode.style.opacity = 1;
    loopKlammern.style.opacity = 1;
    gsap.to(btnAutoplay, {opacity: 0.2, duration: 0.1});
    gsap.to(btnNext, {opacity: 0.2, duration: 0.1});
    gsap.to(btnStop, {opacity: 0.2, duration: 0.1});
    btnAutoplay.removeEventListener("click", autoplay);
    btnNext.removeEventListener("click", nextStep);
    tlDemo.pause();
  }
  if(thisBar > nextBar) {
    arraySwap(arrayDemo, barcounterDemo);
    tlDemo
        .to(iconVergleichen, {opacity: 1})
        .to(vergleichCode, {opacity: 1}, "<")
        .to(vergleichCodeKlammern, {opacity: 1}, "<")
        .to(nextDiv, {backgroundColor: highlightColor}, "<")
        .to(thisDiv, {backgroundColor: highlightColor}, "<")
        .to(nextDiv, {scale: 0, transformOrigin: "bottom center", duration: 0.5, ease: "circ.out"}, "> 1")
        .to(thisDiv, {scale: 0, transformOrigin: "bottom center", duration: 0.8, ease: "circ.out"}, "<")
        .to(iconTauschen, {opacity: 1}, "<")
        .to(tauschCode, {opacity: 1}, "<")
        .to(iconVergleichen, {opacity: 0.1}, "<")
        .to(vergleichCode, {opacity: 0.4}, "<")
        .to(vergleichCodeKlammern, {opacity: 0.4}, "<")
        .call(swapBars, [thisDiv, nextDiv], ">0.5")
        .to(nextDiv, {duration: 0.5, scale: 1, transformOrigin: "bottom center", backgroundColor: "", ease: "circ.out"}, ">")
        .to(thisDiv, {duration: 0.5, scale: 1, transformOrigin: "bottom center", backgroundColor: "", ease: "circ.out"}, "<")
        .to(iconTauschen, {opacity: 0.1}, ">")
        .to(tauschCode, {opacity: 0.4}, "<")
  } else {
    tlDemo
        .to(iconVergleichen, {opacity: 1})
        .to(vergleichCode, {opacity: 1}, "<")
        .to(vergleichCodeKlammern, {opacity: 1}, "<")
        .to(thisDiv, {backgroundColor: highlightColor})
        .to(nextDiv, {backgroundColor: highlightColor}, "<")
        .set(thisDiv, {backgroundColor: ""}, "> 1")
        .set(nextDiv, {backgroundColor: ""}, "<")
        .to(iconVergleichen, {opacity: 0.1}, ">")
        .to(vergleichCode, {opacity: 0.4}, "<")
        .to(vergleichCodeKlammern, {opacity: 0.4}, "<")
  }
  if(barcounterDemo < lastIndexDemo) {
    barcounterDemo++;
  } else if(lastIndexDemo >= 0) {
    barcounterDemo = 0;
    lastIndexDemo--;
  }
}

//tauscht Werte im Array
function arraySwap(array, i) {
  const temp = array[i];
  array[i] = array[i +1];
  array[i+1] = temp;
}

//Tauscht Balken im DOM
function swapBars(node1, node2) {
  const parent = node1.parentNode;
  const oldNode = parent.removeChild(node1);
  node2.after(oldNode);
}

//wertet Entscheidung vom User aus, animiert Übung
function animiereUebung() {
  barsUebung = uebungSection.querySelectorAll(".bar");
  thisBar = arrayUebung[barcounterUebung];
  nextBar = arrayUebung[barcounterUebung + 1];
  thisDiv = barsUebung[barcounterUebung];
  nextDiv = barsUebung[barcounterUebung + 1];
  let textDemo = document.getElementById("text-fertig-sortiert");

  tlUebung.eventCallback("onComplete", function() {
    gsap.to(btnSwap, {opacity: 1, duration: 0.1})
    gsap.to(btnDontSwap, {opacity: 1, duration: 0.1})
  });

  if (lastIndexUebung === 0) {
    gsap.to(barsUebung[0], {backgroundColor: "", duration: 0.5, ease: "circ.out"})
    gsap.to(barsUebung[1], {backgroundColor: "", duration: 0.5, ease: "circ.out"}, "<")
    tlUebung.pause();
    gsap.to(textDemo, {opacity:1});
    gsap.to(btnSwap, {opacity: 0.2, duration: 0.1});
    gsap.to(btnDontSwap, {opacity: 0.2, duration: 0.1});
    return false;
  }
  if(thisBar > nextBar && choice === 1) {
    arraySwap(arrayUebung,barcounterUebung);
    tlUebung
        .to(nextDiv, {scale: 0, transformOrigin: "bottom center", duration: 0.3, ease: "circ.out"}, "> 0.3")
        .to(thisDiv, {scale: 0, transformOrigin: "bottom center", duration: 0.3, ease: "circ.out"}, "<")
        .call(swapBars, [thisDiv, nextDiv], "> 0.3")
        .to(nextDiv, {duration: 0.5, scale: 1, transformOrigin: "bottom center", backgroundColor: "", ease: "circ.out"}, ">")
        .to(thisDiv, {duration: 0.5, scale: 1, transformOrigin: "bottom center", backgroundColor: "", ease: "circ.out"}, "<")

    if(barcounterUebung < lastIndexUebung) {
      barcounterUebung++;
    } else if(lastIndexUebung >= 0) {
      barcounterUebung = 0;
      lastIndexUebung--;
    }
    return true;

  } else if(thisBar <= nextBar && choice === 2) {
    tlUebung
        .to(thisDiv, {backgroundColor: "",duration: 0.3, ease: "circ.out"}, "> 0.3")
        .to(nextDiv, {backgroundColor: "",duration: 0.3, ease: "circ.out"}, "<")
    if(barcounterUebung < lastIndexUebung) {
      barcounterUebung++;
    } else if(lastIndexUebung >= 0) {
      barcounterUebung = 0;
      lastIndexUebung--;
    }
    return true;

  }else if(thisBar > nextBar && choice === 2) {
    displayMistake();
    return false;
  }else if(thisBar <= nextBar && choice === 1) {
    displayMistake();
    return false;
  }
}

//zeigt falsche Eingabe an
function displayMistake() {
  let x = document.getElementById("snackbar");
  x.className = "show";
  setTimeout(function() {x.className = x.className.replace("show", ""); }, 3000);
}

//markiert Bars in der Übung
function highlightBars(node1, node2) {
  if(lastIndexUebung > -1) {
    tlUebung
        .to(node1, {backgroundColor: highlightColor , ease: "circ.out", duration: 0.3}, "> 0.3" )
        .to(node2, {backgroundColor: highlightColor , ease: "circ.out", duration: 0.3},"<")
  }
}